# Dockerfiles



## Setup lingkungan docker ready

 - update dan upgrade package yang sudah terinstall
 - install Git
 - install Docker

 Jika diperlukan, clone dahulu project yang akan dijalankan dengan docker

## Membuat docker image
 

```
docker build . -t <nama docker image boleh pake slash (/)> -f <nama dockerfile> 
```

## Menjalankan docker image dengan detach mode

Detach mode arti nya terminal bisa digunakan untuk keperluan lain setelah menjalankan docker image

```
docker run -d -p <nomor port host>:<nomor port container> --name <nama container> <nama docker image>
```

Jika ingin melihat docker image yang tersedia

```
docker images
```

Untuk melihat docker container yang aktif

```
docker ps
```
 

## Mengupdate isi docker image

- Mematikan docker container

```
docker kill <nama container>
```

- Menghapus docker container

```
docker rm <nama container>
```

- Menghapus docker image

```
docker rmi <nama image>
```

- Ubah file seperti biasa
- Buat lagi docker image
- Jalankan lagi docker image


# Devcontainer

## Setting developer environment untuk gh Codespaces

gh Codespaces merupakan layanan cloud development environment (CDE) yang disediakan oleh github.
gitlab juga punya CDE namanya workspace. bedanya, gh Codespaces gratis. dengan CDE developer bisa menjalankan project nya tanpa perlu local instal. termasuk juga jika hendak menjalankan project dengan docker.

## Devcontainer untuk Cypress e2e framework

Cypress merupakan framework node.js yang digunakan untuk melakukan end-to-end frontend testing. jadi merupakan bagian dari QA automation testing. untuk bisa menjalankan Cypress dengan docker bisa dilihat di folder /devcontainer.example/cypress

perintahnya sbb

```
docker run -it -v $PWD:/e2e -w /e2e cypress/included:12.11.0
```

perintah ini akan otomatis menjalankan Cypress dalam docker dengan posisi file spec di folder /e2e/cypress/e2e/*.cy.js

## Devcontainer untuk dotnet core

dotnet core merupakan ... ah kalian pasti udah paham. setting devcontainer ini ada di folder /devcontainer.example/dotnet-core

perintah nya ... gak ada udah otomatis jalan wkwkwk
